package sheridan;

public class EmailValidator {
	
	public static boolean isValidEmail(String email) {
		boolean flagFormat = false;
		boolean flagAt = false;
		boolean flagAcc = false;
		boolean flagDom = false;
		boolean flagExt = false;
		boolean flagValid = false;
		
		flagFormat = validateFormat(email);
		flagAt = validateAt(email);
		flagAcc = validateAcc(email);
		flagDom = validateDomain(email);
		flagExt = validateExtension(email);
		
		if(!flagFormat||!flagAt||!flagAcc) {
			flagValid = false;
		} 
		else if(!flagDom|| !flagExt ) 
		{
			flagValid = false;
		}
		else {
			flagValid = true;
		}
		
		
		return flagValid;
	}
		
	
	
	public static boolean validateFormat(String email) {
		boolean flag = true;
		int atCounter = 0;
		int periodCounter = 0;

		for (int i = 0; i < email.length(); i++) {
			if ("@".indexOf(email.charAt(i)) != -1) {
				atCounter++;
			}
		}

		for (int i = 0; i < email.length(); i++) {
			if (".".indexOf(email.charAt(i)) != -1) {
				periodCounter++;
			}
		}

		if ((periodCounter != 1) || (atCounter != 1)) {
			flag = false;
		}

		return flag;

	}

	public static boolean validateAt(String email) {
		boolean flag = true;
		int atCounter = 0;

		for (int i = 0; i < email.length(); i++) {
			if ("@".indexOf(email.charAt(i)) != -1) {
				atCounter++;
			}
		}

		if (atCounter != 1) {
			flag = false;
		}

		return flag;

	}

	public static boolean validateAcc(String email) {
		boolean flag = true;
		int counter = 0;
		int pass = 3;
		if ("abcdefghijklmnopqrstuvqxyz".indexOf(email.charAt(0)) != -1) {
			counter++;
		}

		for (int i = 0; i < email.length(); i++) {
			if ("abcdefghijklmnopqrstuvqxyz1234567890".indexOf(email.charAt(i)) != -1) {
				counter++;
			}
		}

		if (counter <= pass) {
			flag = false;
		}

		return flag;

	}

	public static boolean validateDomain(String email) {
		boolean flag = true;
		int counter = 0;

		for (int i = 0; i < email.length(); i++) {
			if ("abcdefghijklmnopqrstuvqxyz1234567890".indexOf(email.charAt(i)) != -1) {
				counter++;
			}
		}

		if (counter < 3) {
			flag = false;
		}

		return flag;

	}
	
	public static boolean validateExtension(String email) {
		boolean flag = true;
		int counter = 0;
		int numCounter = 0;

		for (int i = 0; i < email.length(); i++) {
			if ("abcdefghijklmnopqrstuvqxyzABCDEFGHIJKLMNOPQRSTUVWXYZ".indexOf(email.charAt(i)) != -1) {
				counter++;
			}
			else {
				numCounter++;
			}
		}

		if (counter < 2 || numCounter!= 0) {
			flag = false;
		}

		return flag;

	}
}
