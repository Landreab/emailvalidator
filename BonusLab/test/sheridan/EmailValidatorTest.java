package sheridan;

import static org.junit.Assert.*;

import org.junit.Test;

public class EmailValidatorTest {

	@Test
	public void testValidateFormatRegular() {
		String pass = "brennin@gmail.com";
		boolean result = EmailValidator.validateFormat(pass);
		assertTrue(result);
	}
	
	@Test
	public void testValidateFormatException() {
		String pass = "brennin@gmailcom";
		boolean result = EmailValidator.validateFormat(pass);
		assertFalse(result);
	}
	
	@Test
	public void testValidateFormatBoundaryOut() {
		String pass = "brennin@@gmail..com";
		boolean result = EmailValidator.validateFormat(pass);
		assertFalse(result);
	}
	
	@Test
	public void testValidateAtRegular() {
		String pass = "test@teststring";
		boolean result = EmailValidator.validateAt(pass);
		assertTrue(result);
	}
	
	@Test
	public void testValidateAtException() {
		String pass = "test@@@@@";
		boolean result = EmailValidator.validateAt(pass);
		assertFalse(result);
	}
	
	@Test
	public void testValidateAtBoundaryOut() {
		String pass = "testTestTest";
		boolean result = EmailValidator.validateAt(pass);
		assertFalse(result);
	}
	
	@Test
	public void testValidateAccountRegular() {
		String pass = "tesTString";
		boolean result = EmailValidator.validateAcc(pass);
		assertTrue(result);
	}
	
	@Test
	public void testValidateAccountException() {
		String pass = "123TESTSTRING";
		boolean result = EmailValidator.validateAcc(pass);
		assertFalse(result);
	}
	
	@Test
	public void testValidateAccountBoundaryIn() {
		String pass = "tes";
		boolean result = EmailValidator.validateAcc(pass);
		assertTrue(result);
	}
	
	@Test
	public void testValidateAccountBoundaryOut() {
		String pass = "teS";
		boolean result = EmailValidator.validateAcc(pass);
		assertFalse(result);
	}
	
	@Test
	public void testValidateDomainRegular() {
		String pass = "123domain123";
		boolean result = EmailValidator.validateDomain(pass);
		assertTrue(result);
	}
	
	@Test
	public void testValidateDomainException() {
		String pass = "ABCDEF";
		boolean result = EmailValidator.validateDomain(pass);
		assertFalse(result);
	}
	
	@Test
	public void testValidateDomainBoundaryIn() {
		String pass = "abc";
		boolean result = EmailValidator.validateDomain(pass);
		assertTrue(result);
	}
	
	@Test
	public void testValidateDomainBoundaryOut() {
		String pass = "abC";
		boolean result = EmailValidator.validateDomain(pass);
		assertFalse(result);
	}
	
	@Test
	public void testValidateExtensionRegular() {
		String pass = "com";
		boolean result = EmailValidator.validateExtension(pass);
		assertTrue(result);
	}
	
	
	@Test
	public void testValidateExtensionBoundaryIn() {
		String pass = "ca";
		boolean result = EmailValidator.validateExtension(pass);
		assertTrue(result);
	}
	
	@Test
	public void testValidateExtensionException() {
		String pass = "213cacaca123";
		boolean result = EmailValidator.validateExtension(pass);
		assertFalse(result);
	}


}
